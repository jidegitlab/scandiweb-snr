locals {
  remote_state_bucket_region = "us-east-1"
  remote_state_bucket        = "scandiweb-sre-remote-state-s3"
  infrastructure_state_file  = "scandiweb-infrastructure.tfstate"

  prefix         = data.terraform_remote_state.infrastructure.outputs.prefix
  common_tags    = data.terraform_remote_state.infrastructure.outputs.common_tags
  vpc_id         = data.terraform_remote_state.infrastructure.outputs.vpc_id
  public_subnets = data.terraform_remote_state.infrastructure.outputs.public_subnets

  rand_num = data.terraform_remote_state.infrastructure.outputs.rand_num
  # private_subnets = data.terraform_remote_state.infrastructure.outputs.private_subnets
}

data "terraform_remote_state" "infrastructure" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.infrastructure_state_file
  }
}
