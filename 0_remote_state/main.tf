locals {
  aws_region = "us-east-1"
  prefix     = "scandiweb-sre-remote-state"
  ssm_prefix = "/org/scandiweb/terraform"
  common_tags = {
    Project   = "scandiweb"
    ManagedBy = "Terraform"
  }
}
