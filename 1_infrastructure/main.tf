locals {
  aws_region = "us-east-1"
  prefix     = "scandiweb"
  vpc_name   = "${local.prefix}-vpc"
  vpc_cidr   = "10.10.0.0/16"
  rand_num   = random_uuid.uuid.result
  common_tags = {
    Project   = "scandiweb"
    ManagedBy = "Terraform"
  }
}

resource "random_uuid" "uuid" {}


