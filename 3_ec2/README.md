<!-- BEGIN_TF_DOCS -->

# Elastic Cloud Compute Instances (EC2)
This module sets up the following AWS services:

* Elastic Cloud Compute (EC2)
## Deployment

```sh
terraform init
terraform plan
terraform apply -auto-approve
```

## Tear down

```sh
terraform destroy -auto-approve
```
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS Region to deploy VPC | `string` | `"us-east-1"` | no |
## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_ec2_sg"></a> [ec2\_sg](#module\_ec2\_sg) | terraform-aws-modules/security-group/aws | n/a |
| <a name="module_varnish_server"></a> [varnish-server\](#module\_varnish-server\) | varnish-server | n/a |
| <a name="module_magento_server"></a> [magento-server\](#module\_magento-server\) | magento-server | n/a |
## Outputs

| Name | Description |
|------|-------------|
| <a name="output_public_subnets"></a> [public\_subnets](#output\_public\_subnets) | VPC public subnets' IDs list |
| <a name="output_private_subnets"></a> [private\_subnets](#output\_private\_subnets) | VPC private subnets' IDs list
| <a name="output_magento_server_instance_id"></a> [magento-server\_id](#output\_magento_server_instance\_id) | magento server instance id |
| <a name="output_varnish_server_instance_id"></a> [varnish-server\_id](#output\_varnish_server_instance\_id) | varnish server instance id |
| <a name="output_magento_server_instance_public_dns"></a> [varnish-server\_id](#output\_magento_server_instance_public_dns) | magentp server dns |
| <a name="output_varnish_server_instance_public_dns"></a> [varnish-server\_id](#output\_varnish_server_instance_public_dns) | varnish server dns |
| <a name="output_magento_server_instance_private_ip"></a> [varnish-server\_id](#output\_magento_server_instance_private_ip) | magento server private ip |
| <a name="output_varnish_server_instance_private_ip"></a> [varnish-server\_id](#output\_varnish_server_instance_private_ip) | varnish server private ip |
## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.11.0 |
| <a name="provider_terraform"></a> [terraform](#provider\_terraform) | n/a |
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.9 |
## Resources

| Name | Type |
|------|------|
| [terraform_remote_state.infrastructure](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

<!-- END_TF_DOCS -->