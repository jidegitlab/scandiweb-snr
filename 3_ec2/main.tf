locals {
  remote_state_bucket_region = "us-east-1"
  remote_state_bucket        = "scandiweb-sre-remote-state-s3"
  infrastructure_state_file  = "scandiweb-infrastructure.tfstate"
  alb_state_file             = "scandiweb-alb.tfstate"

  prefix         = data.terraform_remote_state.infrastructure.outputs.prefix
  common_tags    = data.terraform_remote_state.infrastructure.outputs.common_tags
  vpc_id         = data.terraform_remote_state.infrastructure.outputs.vpc_id
  public_subnets = data.terraform_remote_state.infrastructure.outputs.public_subnets
  # private_subnets = data.terraform_remote_state.infrastructure.outputs.private_subnets

  alb_security_group_id = data.terraform_remote_state.alb.outputs.alb_security_group_id

  mysql_security_group_id = aws_security_group.sql_sg.id

  magento_sg = module.private_instance_sg_magento.security_group_id
  varnish_sg = module.private_instance_sg_varnish.security_group_id
}

data "terraform_remote_state" "infrastructure" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.infrastructure_state_file
  }
}

data "terraform_remote_state" "alb" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.alb_state_file
  }
}

resource "aws_key_pair" "private_server" {
  key_name   = "private_server"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDtzXxbKlUMP6eZGOo/J1RBOJMb7LaRENGDusc0sjB+EzwjxSwZAtqyMuWofi1taJBQP391lfBqEIN1d0zuP54095zymQfyNzdxdgUl8Bi4XCFghW8n0TI/xybQ/IFUAHUxdXciZgd+9LWAF04aebMYk21WN9bq02tX8JmzahCtA8w8vqcEYCFQ3JyfDDYU7E2VbX+GXMZd7RLuWEDmr+yuL6TUUMhqD3drNwgtI6jSjukkbTaiQcT3a4FvQzqmEzjPr3E9sz1x7fnp6CU5/UPpi8QAk88FlN6m6vb1W+Ts3Fj44Jw0Xxjx7BOyXLucYIwwxsuL6+n3QtActWAdhcE7UyznjvrY63Bz7rO4F8kovsF1B3pBKrEajTTo7Qh3BvXPrwfpVDOh5i9/0b63etzNegduvzuibzkKBJKltxAtmFlQBPWw3P8p7LZXLslWmZON+vIpZ/zEgKyKAqbzId1x2UrjObTSep71v4NV52tQ9SRwA8kEEN6ez9VPgfLhl2XIX/ThsTFq044uV0Ool1GFzY18zJTe8HbyO9O5NqaGVHtMoQ8tCp8n2GnY6Z7gJWNZG74c43y9WefzDWFgKIk8tXkT9pwIQFgjpRLHLc1OmrTxP8M8jwlWiR09iS0c6rm3PJJoHGFbZUJ2N7h1I4+VtOdg0AiTBR0rRAgVTb67w== jide@jide-Precision-5510"
}

resource "aws_security_group" "sql_sg" {
  name        = "${local.prefix}-mysql-sg"
  description = "Security group for mysql"
  vpc_id      = local.vpc_id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "mysql_sg"
  }
}

module "private_instance_sg_magento" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.16.0"

  name         = "${local.prefix}-priv-server-sg-magento"
  description  = "Security group for private web servers"
  vpc_id       = local.vpc_id
  egress_rules = ["all-all"]

  computed_ingress_with_cidr_blocks = [
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  number_of_computed_ingress_with_cidr_blocks = 1

  computed_ingress_with_source_security_group_id = [
    {
      rule                     = "http-80-tcp"
      source_security_group_id = local.alb_security_group_id
    },
    {
      rule                     = "mysql-tcp"
      source_security_group_id = local.mysql_security_group_id
    },
    {
      rule                     = "http-80-tcp"
      source_security_group_id = local.varnish_sg
    },
  ]
  number_of_computed_ingress_with_source_security_group_id = 3
}

module "private_instance_sg_varnish" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.16.0"

  name         = "${local.prefix}-priv-server-sg-varnish"
  description  = "Security group for private web servers"
  vpc_id       = local.vpc_id
  egress_rules = ["all-all"]

  computed_ingress_with_cidr_blocks = [
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  number_of_computed_ingress_with_cidr_blocks = 1

  computed_ingress_with_source_security_group_id = [
    {
      rule                     = "http-80-tcp"
      source_security_group_id = local.alb_security_group_id
    },
    {
      rule                     = "http-8080-tcp"
      source_security_group_id = local.alb_security_group_id
    }
  ]
  number_of_computed_ingress_with_source_security_group_id = 2
}

module "magento-server" {
  source                      = "./ec2"
  security_groups             = [local.magento_sg]
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.private_server.key_name
  user_data                   = filebase64("./templates/magento-userdata.tpl")
  prefix                      = "magento"
  vpc_id                      = local.vpc_id
  common_tags                 = local.common_tags
  subnet_id                   = local.public_subnets[0]
  associate_public_ip_address = true
  volume_type                 = "gp2"
  volume_tag_name_value       = "magento"
  volume_id                   = "vol-01bb8c734240ac25b"
}

module "varnish-server" {
  source                      = "./ec2"
  security_groups             = [local.varnish_sg]
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.private_server.key_name
  user_data                   = filebase64("./templates/varnish-userdata.tpl")
  prefix                      = "varnish"
  vpc_id                      = local.vpc_id
  common_tags                 = local.common_tags
  subnet_id                   = local.public_subnets[1]
  associate_public_ip_address = true
  volume_type                 = "gp2"
  volume_tag_name_value       = "varnish"
  volume_id                   = "vol-0dbd581052ed74a7a"
}