output "public_subnets" {
  value = data.terraform_remote_state.infrastructure.outputs.public_subnets
}

# output "private_subnets" {
#   value = data.terraform_remote_state.infrastructure.outputs.private_subnets
# }

output "magento_server_instance_id" {
  value = module.magento-server.server_instance_id
}

output "varnish_server_instance_id" {
  value = module.varnish-server.server_instance_id
}

output "magento_server_instance_private_ip" {
  value = module.magento-server.server_private_ip
}

output "varnish_server_instance_private_ip" {
  value = module.varnish-server.server_private_ip
}