# --- ec2/main.tf ---
data "aws_ami" "ubuntu-18_04" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name = "image-id"
    values = ["ami-0ee23bfc74a881de5"]
  }

  owners = [ "amazon" ]
}
data "aws_ebs_volume" "very_volume" {
  most_recent = true

  filter {
    name   = "volume-type"
    values = [var.volume_type]
  }

  filter {
    name   = "tag:Name"
    values = [var.volume_tag_name_value]
  }

  filter {
    name = "volume-id"
    values = [var.volume_id]
  }
}
resource "aws_instance" "server" {
  ami = data.aws_ami.ubuntu-18_04.id 
  instance_type = var.instance_type
  key_name = var.key_name
  subnet_id = var.subnet_id
  vpc_security_group_ids = var.security_groups
  associate_public_ip_address = var.associate_public_ip_address

  user_data = var.user_data

  root_block_device {
    delete_on_termination = var.delete_block_device
  }
  
  tags = merge(
    {
      Name = "${var.prefix}"
    },
    var.common_tags
  )
}
resource "aws_volume_attachment" "ebs_attach" {
  device_name = "/dev/sdf"
  volume_id   = data.aws_ebs_volume.very_volume.id
  instance_id = aws_instance.server.id
  skip_destroy = true
}
