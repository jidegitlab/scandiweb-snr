variable "security_groups" {
  type = list(string)
  description = "resource security groups"
}
variable "instance_type" {
  default     = "t2.micro"
  description = "default instance type to use incase its omitted"
}
variable "aws_region" {
  default = "us-east-1"
  description = "resource deployment region"
}
variable "subnet_id" {}
variable "key_name" {}
variable "user_data" {
  default = ""
}
variable "prefix" {}
variable "common_tags" {}
variable "vpc_id" {}
variable "associate_public_ip_address" {
  default     = true
  description = "allow dhcp server to provide public ipv4 address to instance"
}

variable "delete_block_device" {
  default = true
  description = "whether to delete root block volume on instance termination"
}

variable "volume_type" {
  default = "gp2"
  description = "type of volume"
}

variable "volume_tag_name_value" {}
variable "volume_id" {}
