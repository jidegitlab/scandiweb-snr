locals {
  # resource_name             = trimsuffix(substr("${local.prefix}-ip-tg", 0, 32), "-")
  alb_arn = data.terraform_remote_state.alb.outputs.alb_arn
  alb_sg  = data.terraform_remote_state.alb.outputs.alb_security_group_id
}
resource "aws_iam_server_certificate" "scandiweb_cert" {
  name             = "scandiweb"
  certificate_body = file("~/self-signed-ca-pub.pem")
  private_key      = file("~/self-signed-ca-priv.pem")

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener_certificate" "https_listener_cert_443" {
  listener_arn    = aws_lb_listener.alb_https_listener_varnish.arn
  certificate_arn = aws_iam_server_certificate.scandiweb_cert.arn
  depends_on      = [aws_iam_server_certificate.scandiweb_cert]
}
# ------- ALB Listener --------
# create a listener on port 80 with redirect action
resource "aws_lb_listener" "alb_http_listener" {
  load_balancer_arn = local.alb_arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

## create a listener on port 443 with forward action
# HTTPS
resource "aws_lb_listener" "alb_https_listener_varnish" {
  load_balancer_arn = local.alb_arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_iam_server_certificate.scandiweb_cert.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.varnish.arn
  }
  depends_on = [aws_iam_server_certificate.scandiweb_cert]
}

resource "aws_lb_listener_rule" "magento-443" {
  listener_arn = aws_lb_listener.alb_https_listener_varnish.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.magento.arn
  }

  condition {
    path_pattern {
      values = ["/media/*", "/static/*"]
    }
  }
}
