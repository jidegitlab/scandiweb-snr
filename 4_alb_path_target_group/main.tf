locals {
  remote_state_bucket_region = "us-east-1"
  remote_state_bucket        = "scandiweb-sre-remote-state-s3"
  infrastructure_state_file  = "scandiweb-infrastructure.tfstate"
  alb_state_file             = "scandiweb-alb.tfstate"
  ec2_state_file             = "scandiweb-ec2.tfstate"

  prefix                     = data.terraform_remote_state.infrastructure.outputs.prefix
  common_tags                = data.terraform_remote_state.infrastructure.outputs.common_tags
  vpc_id                     = data.terraform_remote_state.infrastructure.outputs.vpc_id
  public_subnets             = data.terraform_remote_state.infrastructure.outputs.public_subnets
  magento_server_instance_id = data.terraform_remote_state.ec2.outputs.magento_server_instance_id
  varnish_server_instance_id = data.terraform_remote_state.ec2.outputs.varnish_server_instance_id

  magento_server_instance_private_ip = data.terraform_remote_state.ec2.outputs.magento_server_instance_private_ip
  varnish_server_instance_private_ip = data.terraform_remote_state.ec2.outputs.varnish_server_instance_private_ip
}

data "terraform_remote_state" "infrastructure" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.infrastructure_state_file
  }
}

data "terraform_remote_state" "alb" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.alb_state_file
  }
}

data "terraform_remote_state" "ec2" {
  backend = "s3"
  config = {
    bucket = local.remote_state_bucket
    region = local.remote_state_bucket_region
    key    = local.ec2_state_file
  }
}