terraform {
  backend "s3" {
    bucket         = "scandiweb-sre-remote-state-s3"
    key            = "scandiweb-alb-path-target-group.tfstate"
    region         = "us-east-1"
    encrypt        = "true"
    dynamodb_table = "scandiweb-sre-remote-state-dynamodb"
  }
}
