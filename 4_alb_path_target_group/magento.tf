locals {
  magento_server_name = "magento"
}

resource "aws_lb_target_group_attachment" "magento" {
  target_group_arn = aws_lb_target_group.magento.arn
  target_id        = local.magento_server_instance_id
  # target_id        = local.magento_server_instance_private_ip
  port = 80
}

resource "aws_lb_target_group" "magento" {
  name     = local.magento_server_name
  port     = 80
  protocol = "HTTP"
  vpc_id   = local.vpc_id
  # target_type = "ip"

  health_check {
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 30
  }

  tags = merge(
    {
      Name = local.magento_server_name
    },
    local.common_tags
  )
}