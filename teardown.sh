#!/usr/bin/bash
green='\033[32;40m'
dir=$(pwd)

set -e #stop if it fails at any point

# #loop backwards
# for file in $(ls -t $dir)
#     do
#     if [ -d "$file" ]
#         then
#         echo $file
#         cd $file
#         terraform destroy -auto-approve -lock=false
#     fi
# done | sort -r

cd 4_alb_path_target_group && terraform destroy -auto-approve -lock=false && cd .. &&
cd 3_ec2 && terraform destroy -auto-approve -lock=false && cd .. &&
cd 2_alb && terraform destroy -auto-approve -lock=false && cd .. &&
cd 1_infrastructure && terraform destroy -auto-approve -lock=false && cd .. &&
cd 0_remote_state && terraform destroy -auto-approve -lock=false

echo -ne "$green DESTRUCTION COMPLETE!!!"