<!-- BEGIN_TF_DOCS -->
# This project deploys AWS infrastructure to AWS.

## Deployment

```sh
bash setup.sh
```

## Tear down

```sh
bash teardown.sh
```

## SSH into Magento and Varnish Server


Add private instance key to running ssh-agent:
```sh 
ssh-add 'private_server.pem'
```
ssh into host with private key running in the background

```sh
ssh ubuntu@<private instance private ip>
```

## Providers
aws

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Resources
AWS S3  
AWS DYNAMODB  
AWS VPC  
AWS ALB  
AWS S3   
AWS EC2
<!-- END_TF_DOCS -->